;;; Main pyton configuration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set the number to the number of columns to use.
(setq-default fill-column 80)
;; Don't use TABS for indentations.
(setq-default indent-tabs-mode nil)
;; Remove trailing whitespace manually by typing C-t C-w.
(add-hook 'python-mode-hook
          (lambda ()
            (local-set-key (kbd "C-t C-w")
                           'delete-trailing-whitespace)))
;; Automatically remove trailing whitespace when file is saved.
(add-hook 'python-mode-hook
      (lambda()
        (add-hook 'local-write-file-hooks
              '(lambda()
                 (save-excursion
                   (delete-trailing-whitespace))))))

;; ;; Highlight character at "fill-column" position.
;; (require 'column-marker)
;; (set-face-background 'column-marker-1 "red")
;; (add-hook 'python-mode-hook
;;           (lambda () (interactive)
;;             (column-marker-1 fill-column)))

;; Jedi
;; (add-hook 'python-mode-hook 'jedi:setup)
;; (setq jedi:complete-on-dot t)

;; Debug
(defun python-add-breakpoint ()
  "Add a break point"
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent)
  (insert "import ipdb; ipdb.set_trace()")

  (highlight-lines-matching-regexp "^[ ]*import ipdb; ipdb.set_trace()"))

(require 'python)
(define-key python-mode-map (kbd "C-c C-b") 'python-add-breakpoint)
(define-key python-mode-map (kbd "C-c e") 'flymake-popup-current-error-menu)

(defun python-get-buffer-filename-as-module (buffername)
  "Insert the path to the python module represented by BUFFERNAME"
  (interactive "b")
  (unless (string= (f-ext buffername) "py")
    (error "%s is not a python module" buffername))
  (let* ((path (f-short (buffer-file-name (get-buffer buffername))))
         (project-root (f-short (projectile-root-bottom-up path '("manage.py"))))
         (module-unix-path (s-chop-prefix project-root path))
         (module-python-path (s-replace "/" "." (f-no-ext module-unix-path))))
    (insert module-python-path)))

;; Treat jinja as htlm
(add-to-list 'auto-mode-alist '("\\.jinja\\'" . html-mode))

;; Yasnippets
(yas-minor-mode t)
(yas-minor-mode-on)
(global-set-key (kbd "C-c .") 'yas/expand)

;; Pyflakes
(require 'flymake-python-pyflakes)
(add-hook 'python-mode-hook 'flymake-python-pyflakes-load)
(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'python-mode-hook 'pretty-mode)

(defun python-doc ()
  (interactive)
  (setq-local helm-dash-docsets '("Python" "Django"))
  (global-set-key (kbd "M-!") 'helm-dash-at-point)
  )

(add-hook 'python-mode-hook 'python-doc)

;; Anaconda
;; M-.	anaconda-mode-goto-definitions
;; M-*	anaconda-nav-pop-marker
;; M-?	anaconda-mode-view-doc
;; M-r	anaconda-mode-usages

(global-set-key (kbd "M-,") 'anaconda-nav-pop-marker)
(add-hook 'python-mode-hook 'anaconda-mode)

(provide 'python-dev)
