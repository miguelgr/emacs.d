;; Shell Configuration
(setq shell-file-name "/bin/zsh")

;; Bash completion
(autoload 'bash-completion-dynamic-complete
  "bash-completion"
  "BASH completion hook")

(add-hook 'shell-dynamic-complete-functions
  'bash-completion-dynamic-complete)

(provide 'shell)
