(autoload 'js3-mode "js3" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js3-mode))
(add-hook 'js3-mode-hook
          (lambda ()
            (setq js3-auto-indent-p t
                  js3-curly-indent-offset 0
                  js3-enter-indents-newline t
                  js3-expr-indent-offset 2
                  js3-indent-on-enter-key t
                  js3-lazy-commas t
                  js3-lazy-dots t
                  js3-lazy-operators t
                  js3-paren-indent-offset 2
                  js3-square-indent-offset 4)))

(defun javascript-add-breakpoint ()
  "Add a break point"
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent)
  (insert "debugger;")

  (highlight-lines-matching-regexp "^[ ]*debugger;"))

(require 'flymake-jslint)
(add-hook 'js-mode-hook 'flymake-jslint-load)

(global-set-key (kbd "C-c C-b") 'javascript-add-breakpoint)

(provide 'javascript)
