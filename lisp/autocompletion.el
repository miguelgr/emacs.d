(require 'auto-complete)
(global-auto-complete-mode)
(auto-complete-mode)

(provide 'autocompletion)
