;; EMACSEN ;;
(when (version< emacs-version "24.1")
  (error "Configuration needs at least GNU Emacs 24.1. You are using %s" emacs-version))

;; My stuff
(defvar my-dir
  "~/.emacs.d"
  "The root dir of the Emacs distribution.")


(defvar my-org-dir
  "~/Notes"
  "Directory to store all org TODO files.")


(defvar my-modes-mode-line
  '((emacs-lisp . "ε")
    (projectile . "Π")
    (flycheck . "Fy"))
  "Symbol to use in the mode line for certain modes.")


;; PACKAGES
(add-to-list 'load-path
             "~/.emacs.d/packages")

;; Cask and Pallet dependecy and package management
(require 'cask "~/.cask/cask.el")
(cask-initialize)

(require 'pallet)
(pallet-mode t)

;; Custom configuration
(add-to-list 'load-path "~/.emacs.d/lisp")

;; Initialize custom configuration
(require 'appearance)
(require 'editor)
(require 'autocompletion)
(require 'shell)
(require 'python-dev)
(require 'javascript)

;; Emacs server mode always on
;; emacsclient -t/-c
(server-mode)
(server-start)

;; Packages
(require 'git-gutter)
(require 'resize-window)

;; Expand Region
(require 'expand-region)
(global-set-key (kbd "C-@") 'er/expand-region)
(global-set-key (kbd "C-q") 'kill-region)p

(global-set-key (kbd "M-o") 'other-window)


;; ;; Yasnippets
(require 'yasnippet)
(add-to-list 'load-path
              "~/.emacs.d/packages/yasnippet")
;; (yas-global-mode 1)

;; Autocomplete
(auto-complete-mode)

;; Smart mode Line ;;
(sml/setup)
(setq sml/no-confirm-load-theme -1)
(sml/apply-theme 'automatic)

(require 'achievements)
(achievements-mode t)

;; Discover mode
(require 'discover)
(global-discover-mode 1)

;; Web mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))


(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq-default indent-tabs-mode nil)
  (setq web-mode-enable-auto-pairing nil)
  (setq web-mode-enable-current-element-highlight t)
  (web-mode-set-engine "django")
  (setq web-mode-enable-current-column-highlight t)
  (global-set-key (kbd "C-c e") 'emmet-expand-line)

)
(add-hook 'web-mode-hook  'my-web-mode-hook)

(require 'move-text)
(move-text-default-bindings)

;;Ido
(ido-mode 1)
(ido-everywhere 1)
(ido-vertical-mode 1)
(setq ido-enable-flex-matching -1)

;; Maggit
(global-set-key (kbd "C-c m s") 'magit-status)
(global-set-key (kbd "C-c m b") 'magit-blame)
(global-set-key (kbd "C-c m d") 'magit-diff)
(global-set-key (kbd "C-c m l") 'magit-log)

;; Search in Google
(require 'google-this)
(google-this-mode 1)

;; Flycheck
(require 'flycheck)
(global-flycheck-mode)

(flycheck-define-checker proselint
  "A linter for prose."
  :command ("proselint" source-inplace)
  :error-patterns
  ((warning line-start (file-name) ":" line ":" column ": "
            (id (one-or-more (not (any " "))))
            (message (one-or-more not-newline)
                     (zero-or-more "\n" (any " ") (one-or-more not-newline)))
            line-end))
  :modes (text-mode markdown-mode gfm-mode))
(add-to-list 'flycheck-checkers 'proselint)

;;Tramp Mode (Transparent Remote Access, Multiple Protocols)
;; (setq tramp-default-method "ssh")

;; Multiples Cursor configuration
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-M->") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-M-<") 'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-M-<") 'mc/mark-pop)
;; MC: All
(global-set-key (kbd "C-c C-<") 'mc/mark-all-in-region)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)
(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/ad-cursor-on-click)

;; Projectile + Helm
(projectile-global-mode)
(setq projectile-completion-system 'helm)
(helm-projectile-on)

;; Helm
(global-set-key (kbd "C-c P") 'helm-projectile-find-file)
(global-set-key (kbd "C-c S") 'helm-semantic-or-imenu)
(global-set-key (kbd "C-c p h") 'helm-projectile)
(global-set-key (kbd "M-p") 'helm-buffers-list)
(global-set-key (kbd "s-x") 'helm-M-x)
(global-set-key (kbd "s-?") 'helm-ag)

;; Ace mode
(global-set-key (kbd "M-(") 'ace-jump-word-mode)
(global-set-key (kbd "M-)") 'ace-jump-line-mode)


(defun go-doc ()
  (interactive)
  (setq-local helm-dash-docsets '("Go")))

(global-set-key (kbd "s-/") 'helm-dash-at-point)
(add-hook 'go-mode-hook 'go-doc)

(window-numbering-mode)
(autoload 'bash-completion-dynamic-complete
  "bash-completion"
  "BASH completion hook")
(add-hook 'shell-dynamic-complete-functions
  'bash-completion-dynamic-complete)

;; Random xkcd comic
;; (xkcd-mode)
;; (xkcd-rand)
(setq org-agenda-files (list "~/Notes/work.org"
                             "~/Notes/meryl.org"
                             "~/Notes/home.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "49ad7c8d458074db7392f8b8a49235496e9228eb2fa6d3ca3a7aa9d23454efc6" "28ec8ccf6190f6a73812df9bc91df54ce1d6132f18b4c8fcc85d45298569eb53" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(sml/no-confirm-load-theme nil)
 '(sml/pos-minor-modes-separator (quote |))
 '(sml/shorten-modes t)
 '(sml/theme (quote dark))
 '(sml/vc-mode-show-backend t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil)))))
